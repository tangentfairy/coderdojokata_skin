<div class="kata-mentors">

	<div class="row kata-box">
		<div class="kata-breadcrumbs">
			<div class="kata-breadcrumb">Categories</div>
			<div class="kata-breadcrumb">Design</div>
		</div>
	</div>
	<div class="row kata-mentors-group">
		<div class="col-xs-12 kata-mentors-filtercontent">
			<table class="kata-filter-section">
				<tr>
					<th class="kata-filter-element">Skill Level</th>
					<th class="kata-filter-element">Software</th>
					<th class="kata-filter-element">Authors</th>
					<th class="kata-filter-element">Ratings</th>
					<th class="kata-filter-element"></th>
				</tr>
				<tr>
					<td class="kata-filter-element"><select class="kata-select"><option>Beginner</option></select></td>
					<td class="kata-filter-element"><select class="kata-select"><option>Photoshop</option></select></td>
					<td class="kata-filter-element"><select class="kata-select"><option>Bill
								Lowe</option></select></td>
					<td class="kata-filter-element"><select class="kata-select"><option>*****</option></select></td>
					<td class="kata-filter-element"><button class="kata-button">Apply</button></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row kata-mentors-group kata-top-margin-2rem">
		<div class="col-xs-12 kata-mentors-filtercontent">
			<table class="kata-result-section">
				<tr>
					<th class="kata-result-element">Name:</th>
					<th class="kata-result-element">Author:</th>
					<th class="kata-result-element-description">Description:</th>
					<td class="kata-result-element" rowspan="6">IMAGE</td>

				</tr>
				<tr>
					<td class="kata-result-element">HTML Course</td>
					<td class="kata-result-element">Bill Lowe</td>
					<td class="kata-result-element-description" rowspan="5">Lorem ipsum
						dolor sit amet, consectetur adipisci elit, sed eiusmod tempor
						incidunt ut labore et dolore magna aliqua. Ut enim ad minim
						veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid
						ex ea commodi consequat. Quis aute iure reprehenderit in voluptate
						velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
						obcaecat cupiditat non proident, sunt in culpa qui officia
						deserunt mollit anim id est laborum.</td>
				</tr>
				<tr>
					<th class="kata-result-element">Skill Level:</th>
					<th class="kata-result-element">Sessions:</th>
				</tr>
				<tr>
					<td class="kata-result-element">Beginner</td>
					<td class="kata-result-element">10</td>
				</tr>
				<tr>
					<th class="kata-result-element" colspan="2">Category</th>
				</tr>
				<tr>
					<td class="kata-result-element" colspan="2">Mobile</td>
				</tr>
				<tr>
					<td class="kata-result-element" colspan="3"></td>
					<td class="kata-result-element"><button class="kata-button">Start
							Tutorial</button></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row kata-mentors-group kata-top-margin-2rem">
		<div class="col-xs-12 kata-mentors-filtercontent">
			<table class="kata-result-section">
				<tr>
					<th class="kata-result-element">Name:</th>
					<th class="kata-result-element">Author:</th>
					<th class="kata-result-element-description">Description:</th>
					<td class="kata-result-element" rowspan="6">IMAGE</td>

				</tr>
				<tr>
					<td class="kata-result-element">HTML Course</td>
					<td class="kata-result-element">Bill Lowe</td>
					<td class="kata-result-element-description" rowspan="5">Lorem ipsum
						dolor sit amet, consectetur adipisci elit, sed eiusmod tempor
						incidunt ut labore et dolore magna aliqua. Ut enim ad minim
						veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid
						ex ea commodi consequat. Quis aute iure reprehenderit in voluptate
						velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
						obcaecat cupiditat non proident, sunt in culpa qui officia
						deserunt mollit anim id est laborum.</td>
				</tr>
				<tr>
					<th class="kata-result-element">Skill Level:</th>
					<th class="kata-result-element">Sessions:</th>
				</tr>
				<tr>
					<td class="kata-result-element">Beginner</td>
					<td class="kata-result-element">10</td>
				</tr>
				<tr>
					<th class="kata-result-element" colspan="2">Category</th>
				</tr>
				<tr>
					<td class="kata-result-element" colspan="2">Mobile</td>
				</tr>
				<tr>
					<td class="kata-result-element" colspan="3"></td>
					<td class="kata-result-element"><button class="kata-button">Start
							Tutorial</button></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="row kata-mentors-group kata-top-margin-2rem">
		<div class="col-xs-12 kata-mentors-filtercontent">
			<table class="kata-result-section">
				<tr>
					<th class="kata-result-element">Name:</th>
					<th class="kata-result-element">Author:</th>
					<th class="kata-result-element-description">Description:</th>
					<td class="kata-result-element" rowspan="6">IMAGE</td>

				</tr>
				<tr>
					<td class="kata-result-element">HTML Course</td>
					<td class="kata-result-element">Bill Lowe</td>
					<td class="kata-result-element-description" rowspan="5">Lorem ipsum
						dolor sit amet, consectetur adipisci elit, sed eiusmod tempor
						incidunt ut labore et dolore magna aliqua. Ut enim ad minim
						veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid
						ex ea commodi consequat. Quis aute iure reprehenderit in voluptate
						velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
						obcaecat cupiditat non proident, sunt in culpa qui officia
						deserunt mollit anim id est laborum.</td>
				</tr>
				<tr>
					<th class="kata-result-element">Skill Level:</th>
					<th class="kata-result-element">Sessions:</th>
				</tr>
				<tr>
					<td class="kata-result-element">Beginner</td>
					<td class="kata-result-element">10</td>
				</tr>
				<tr>
					<th class="kata-result-element" colspan="2">Category</th>
				</tr>
				<tr>
					<td class="kata-result-element" colspan="2">Mobile</td>
				</tr>
				<tr>
					<td class="kata-result-element" colspan="3"></td>
					<td class="kata-result-element"><button class="kata-button">Start
							Tutorial</button></td>
				</tr>
			</table>
		</div>
	</div>

	<div>&nbsp;</div>
</div>