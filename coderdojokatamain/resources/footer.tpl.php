
<footer id="footer">
	<div class="subscribe-block">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="text-block">
						<strong class="title">Sign up for our Newsletter</strong>
						<p class="hidden-xs">Get the top news stories and more sent
							directly to your inbox.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<!-- Form by MailChimp for WordPress plugin v2.1.2 - https://mc4wp.com/ -->
					<form method="post" action="https://coderdojo.org/"
						id="mc4wp-form-1" class="form mc4wp-form">
						<div class="subscribe-form row">

							<div class="col-xs-12 col-sm-6" style="padding-right: 0;">
								<input type="text" name="FNAME" placeholder="First name">
							</div>

							<div class="col-xs-12 col-sm-6" style="padding-left: 1px;">
								<input type="text" name="LNAME" placeholder="Last name">
							</div>

							<div class="col-xs-12 col-sm-12">
								<input type="email" id="mc4wp_email" name="EMAIL"
									placeholder="Your email address" required=""> <input
									type="submit" value="Sign up">
							</div>

						</div>
						<textarea name="_mc4wp_required_but_not_really"
							style="display: none !important;"></textarea>
						<input type="hidden" name="_mc4wp_form_submit" value="1"><input
							type="hidden" name="_mc4wp_form_instance" value="1"><input
							type="hidden" name="_mc4wp_form_nonce" value="f6d867a9af">
					</form>
					<!-- / MailChimp for WP Plugin -->
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-5 pull-right">
				<ul class="social-networks">
					<li class="flickr"><a
						href="https://www.flickr.com/photos/helloworldfoundation/">Flickr</a></li>
					<li class="google"><a
						href="https://plus.google.com/+coderdojo/posts">Google+</a></li>
					<li class="linkedin"><a
						href="https://www.linkedin.com/company/5178906?trk=tyah&amp;trkInfo=tarId%3A1409649959259%2Ctas%3Acoderdojo%2Cidx%3A3-1-3">Linkedin</a></li>
					<li class="facebook"><a href="https://www.facebook.com/CoderDojo">Facebook</a></li>
					<li class="twitter"><a href="https://twitter.com/coderdojo">Twitter</a></li>
				</ul>
				<form method="get" class="search-form"
					action="https://coderdojo.org">
					<fieldset>
						<div class="form-group">
							<input type="search" class="form-control" placeholder="Search"
								name="s" value="">
						</div>
					</fieldset>
				</form>
			</div>
			<div class="col-sm-3">
				<div class="logo hidden-xs">
					<a href="https://coderdojo.org"><img
						src="https://coderdojo.org/wp-content/themes/coderdojoorg/images/logo.png"
						alt="CoderDojo.org"></a>
				</div>
				<address>
					<span>Dogpatch Labs,</span> <span>35 Barrow Street,</span> <span>Dublin
						4,</span> Ireland
				</address>
			</div>
			<div class="col-sm-4">
				<dl>
					<dt>General Enquiries:</dt>
					<dd>
						<a href="mailto:info@coderdojo.com">info@coderdojo.com</a>
					</dd>
					<dt>Partner Enquiries:</dt>
					<dd>
						<a href="mailto:partner@coderdojo.com">partner@coderdojo.com</a>
					</dd>
				</dl>
				<a class=" hidden-xs" href="http://www.coderdojo.com">CoderDojo.com</a><span
					class="visit">Visit <a href="http://www.coderdojo.com">CoderDojo.com</a></span>
			</div>
		</div>
	</div>
</footer>
